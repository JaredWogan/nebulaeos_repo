#!/bin/bash

rm ./NebulaeOS_Repo*

echo
echo "####################################"
echo "     Updating the repository...     "
echo "####################################"
echo
repo-add -n -R NebulaeOS_Repo.db.tar.gz *.pkg.tar.zst
sleep 5

rm NebulaeOS_Repo.db
rm NebulaeOS_Repo.db.sig

rm NebulaeOS_Repo.files
rm NebulaeOS_Repo.files.sig

mv NebulaeOS_Repo.db.tar.gz NebulaeOS_Repo.db
mv NebulaeOS_Repo.db.tar.gz.sig NebulaeOS_Repo.db.sig

mv NebulaeOS_Repo.files.tar.gz NebulaeOS_Repo.files
mv NebulaeOS_Repo.files.tar.gz.sig NebulaeOS_Repo.files.sig

echo
echo "####################################"
echo "          Repo Updated!!            "
echo "####################################"
echo
