@echo off
:: Get current DIRECTORY
for %%I in (.) do set CurrDirName=%%~nxI

:: Pull down latest version of the files first
echo.
echo ################################################################
echo        Making sure the latest files are downloaded...
echo ################################################################
echo.
git pull origin master

:: Add all changes to the git commit
echo.
echo ################################################################
echo            Adding all changes to the repository...
echo ################################################################
echo.
git add --all .

:: Commit the changes
git commit -m "Update"

:: Push the changes to the remote repository
echo.
echo ################################################################
echo          Pushing changes to the remote repository...
echo ################################################################
echo.
git push -u origin master

echo.
echo ################################################################
echo ###################    Git Push Done      ######################
echo ################################################################